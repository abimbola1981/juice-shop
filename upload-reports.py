import requests
import sys
import os

file_name = sys.argv[1]
scan_type = ''

if file_name == 'gitleaks.json':
    scan_type = 'Gitleaks Scan'
elif file_name == 'njsscan.sarif':
    scan_type = 'SARIF'
elif file_name == 'semgrep.json':
    scan_type = 'Semgrep JSON Report'
elif ile_name == 'retire.json':
    scan_type = 'Retire.js Scan'

headers = {
    'Authorization': 'Token 71f1c8d1525f06c38bc665c60e36fcb65b887829'
}

url = 'https://demo.defectdojo.org/api/v2/import-scan/'

data = {
    'active': True,
    'verified': True,
    'scan_type': scan_type,
    'minimum_severity': 'low',
    'engagement': 14,
}

# Check if the file exists before attempting to open it
if os.path.exists(file_name):
    files = {file_name: open(file_name, 'rb')}
    response = requests.post(url, headers=headers, data=data, files=files)
    
    if response.status_code == 201:
        print('Scan results Upload successful')
    else:
        print(f'Scan results Upload failed: {response.content.decode()}')
else:
    print(f'File {file_name} not found.')
